/**
 * @author Ari Ardiansyah
 * @package binar-academy (kampus-merdeka)
 * @description challenge-1
 */

const readline = require("readline")
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

class Console {
  static main(){
    console.log("🚀 Daftar Pilihan: ")
    console.log('-----------------------------------------------------------')
    console.log("1. Kalkulator sederhana (*, /, +, -)")
    console.log("2. Perhitungan akar kuadrat (√x)")
    console.log("3. Perhitungan pangkat (x^)")
    console.log("4. Luas persegi (cm^2)")
    console.log("5. Volume kubus (cm^3)")
    console.log("6. Volume tabung (cm^3)")
    console.log("7. Keluar dari program")

    console.log('\n')
    const pertanyaan = () => {
      rl.question("Silakan masukan salah satu pilihan diatas: ", (answer) => {
        console.log('\n')
        answer = +answer
        switch (answer) {
          case 1:
            this.kalkulator()
            break;
          case 2:
            this.akar()
            break;
          case 3:
            this.pangkat()
            break;
          case 4:
            this.luas_persegi()
            break;
          case 5:
            this.volume_kubus()
            break;
          case 6:
            this.volume_tabung()
            break;
          case 7:
            this.exit()
            break;
        
          default:
            console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus 1-7")
            // this.main()
            pertanyaan()
            break;
        }
      })
    }

    pertanyaan()
  }

  static exit() {
    rl.close();
  }

  static retry() {
    console.log('\x1b[0m')
    rl.question('Kembali ke menu utama (y/t)? ', (answer) => {
      if(answer.toLocaleLowerCase() == 'y'){
        this.main()
      }else{
        rl.close()
      }
    })
  }

  static kalkulator() {
    console.log("💻 Kalkulator sederhana (*, /, +, -)")
    console.log('-----------------------------------------------------------')
    console.log("Silakan input a = angka, b = angka, c = operator matematika \n")
    
    let angka1, angka2, operator;
    const inputAngka1 = () => {
      rl.question("(a) Silakan masukan angka pertama: ", (answer)=> {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka1 = answer
          inputAngka2();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka1();
        }
      })
    }
    
    const inputAngka2 = () => {
      rl.question("(b) Silakan masukan angka kedua: ", (answer) => {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka2 = answer
          inputOperator();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka2();
        }
      })
    }
    
    const inputOperator = () => {
      rl.question("(c) Silakan masukan operator matematika (*, /, +, -): ", (answer) => {
        let ruleArr = ["*", "/", "+", "-"]
        let validate = (ruleArr.indexOf(answer) > -1)
        
        if(validate){
          operator = answer
          console.log('-----------------------------------------------------------------')
          operasiKalkulator()
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa operator matematika (*, /, +, -)")
          inputOperator()
        }
      })
    }
    
    const operasiKalkulator = () => {
      console.log("Hasil perhitungan: ")
      // console.log(angka1, angka2, operator)
      let calc = `${angka1} ${operator} ${angka2}`
      calc = `✔ ${calc} = ${eval(calc)}`
      console.log('\x1b[32m', calc)
      
      this.retry()
    }

    inputAngka1()
  }

  static akar() {
    console.log("💻 Perhitungan akar kuadrat (√x)")
    console.log('-----------------------------------------------------------')
    console.log("Silakan input a = angka \n")
    
    let angka1;
    const inputAngka1 = () => {
      rl.question("(a) Silakan masukan angka: ", (answer)=> {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka1 = answer
          console.log('-----------------------------------------------------------------')
          operasiKalkulator();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka1();
        }
      })
    }
    
    const operasiKalkulator = () => {
      console.log("Hasil perhitungan: ")
      // console.log(angka1, angka2, operator)
      let calc = `√${angka1}`
      calc = `✔ ${calc} = ${Math.sqrt(angka1)}`
      console.log('\x1b[32m', calc)

      this.retry()
    }

    inputAngka1()
  }

  static pangkat() {
    console.log("💻 Perhitungan pangkat (x^)")
    console.log('-----------------------------------------------------------')
    console.log("Silakan input a = angka, b = angka (untuk pangkat) \n")
    
    let angka1, angka2, operator;
    const inputAngka1 = () => {
      rl.question("(a) Silakan masukan angka: ", (answer)=> {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka1 = answer
          inputAngka2();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka1();
        }
      })
    }
    
    const inputAngka2 = () => {
      rl.question("(b) Silakan masukan pangkat: ", (answer) => {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka2 = answer
          console.log('-----------------------------------------------------------------')
          operasiKalkulator();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka2();
        }
      })
    }
    
    const operasiKalkulator = () => {
      console.log("Hasil perhitungan: ")
      // console.log(angka1, angka2, operator)
      let calc = `${angka1}^${angka2}`
      calc = `✔ ${calc} = ${Math.pow(angka1, angka2)}`
      console.log('\x1b[32m', calc)

      this.retry()
    }

    inputAngka1()
  }

  static luas_persegi() {
    console.log("💻 Luas persegi")
    console.log('-----------------------------------------------------------')
    console.log("Silakan input a = sisi persegi\n")
    
    let angka1, angka2, operator;
    const inputAngka1 = () => {
      rl.question("(a) Silakan masukan sisi persegi dalam cm (s): ", (answer)=> {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka1 = answer
          console.log('-----------------------------------------------------------------')
          operasiKalkulator();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka1();
        }
      })
    }
    
    const operasiKalkulator = () => {
      console.log("Hasil perhitungan: ")
      console.log('\x1b[32m')
      console.log('Dik: ')
      console.log(`✔ s = Sisi, L = Luas Persegi`)
      console.log(`✔ s = ${angka1} cm`)
      console.log('')
      console.log('Dit: ')
      console.log('✔ L = ....? ')
      console.log('')
      console.log('Rumus: ')
      console.log('✔ L = s^2 ')
      console.log('')
      console.log('Jawab: ')
      let calc = `${angka1}^2`
      calc = `✔ L = ${calc}`
      console.log(calc)
      calc = `✔ L = ${Math.pow(angka1, 2)} cm^2`
      console.log(calc)
      
      this.retry()
    }

    inputAngka1()
  }

  static volume_kubus() {
    console.log("💻 Volume kubus")
    console.log('-----------------------------------------------------------')
    console.log("Silakan input a = rusuk\n")
    
    let angka1;
    const inputAngka1 = () => {
      rl.question("(a) Silakan masukan panjang rusuk dalam cm (r): ", (answer)=> {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka1 = answer
          console.log('-----------------------------------------------------------------')
          operasiKalkulator();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka1();
        }
      })
    }
    
    const operasiKalkulator = () => {
      console.log("Hasil perhitungan: ")
      console.log('\x1b[32m')
      console.log('Dik: ')
      console.log(`✔ r = Rusuk, V = Volume Kubus`)
      console.log(`✔ r = ${angka1} cm`)
      console.log('')
      console.log('Dit: ')
      console.log('✔ V = ....? ')
      console.log('')
      console.log('Rumus: ')
      console.log('✔ V = r^3 ')
      console.log('')
      console.log('Jawab: ')
      let calc = `${angka1}^3`
      calc = `✔ V = ${calc}`
      console.log(calc)
      calc = `✔ V = ${Math.pow(angka1, 3)} cm^3`
      console.log(calc)

      this.retry()
    }

    inputAngka1()
  }

  static volume_tabung() {
    console.log("💻 Volume tabung")
    console.log('-----------------------------------------------------------')
    console.log("Silakan input a = jari-jari, b = tinggi\n")
    
    let angka1, angka2;
    const inputAngka1 = () => {
      rl.question("(a) Silakan masukan jari-jari dalam cm (r) : ", (answer)=> {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka1 = answer
          inputAngka2();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka1();
        }
      })
    }

    const inputAngka2 = () => {
      rl.question("(b) Silakan masukan tinggi dalam cm (t) : ", (answer)=> {
        answer = +answer;
        let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '';
        if(validate){
          angka2 = answer
          console.log('-----------------------------------------------------------------')
          operasiKalkulator();
        }else{
          console.log('\x1b[33m%s\x1b[0m', "🛈 Inputan harus berupa angka (Number)")
          inputAngka2();
        }
      })
    }
    
    const operasiKalkulator = () => {
      let phi = 3.14;
      console.log("Hasil perhitungan: ")
      console.log('\x1b[32m')
      console.log('Dik: ')
      console.log(`✔ r = jari-jari, t = tingi tabung, π = ${phi}, V = volume tabung`)
      console.log(`✔ r = ${angka1} cm`)
      console.log(`✔ t = ${angka2} cm`)
      console.log('')
      console.log('Dit: ')
      console.log('✔ V = ....? ')
      console.log('')
      console.log('Rumus: ')
      console.log('✔ V = πr^2t ')
      console.log('')
      console.log('Jawab: ')
      let calc = `3.14*${angka1}^2*${angka2}`
      calc = `✔ V = ${calc}`
      console.log(calc)
      calc = `✔ V = ${Math.round(3.14*(Math.pow(angka1,2))*angka2)} cm^3`
      console.log(calc)

      this.retry()
    }

    inputAngka1()
  }
}

rl.on('close', function () {
  console.log('\nTerima kasih, telah menggunakan 😘');
  process.exit(0);
});

//Start console
Console.main()