const { default: axios } = require('axios');

class Pahlawan {
    constructor(name, birth_year, death_year) {
        this.name = name
        this.birth_year = birth_year
        this.death_year = death_year
        this.age_of_date;
    }

    hitungUmur() {
        this.age_of_date = this.death_year - this.birth_year
    }
}

async function getDataAsyncAwait() {
    try{
        const { data } = await axios('https://indonesia-public-static-api.vercel.app/api/heroes')
        console.log(data)
    }catch(err){
        console.log(err.message)
    }
}

function getDataCallback() {
    axios.get('https://indonesia-public-static-api.vercel.app/api/heroes').then((response) => {
        console.log(response.data)
    }).catch((err) => {
        console.log(err)
    })
}

// async function getDataFetch(){
//     let request = await fetch('https://indonesia-public-static-api.vercel.app/api/heroes')
//     console.log(request)
// }

// getDataAsyncAwait()
// getDataCallback()
getDataFetch()