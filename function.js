const strArray = ['JavaScript', 'Java', 'C', 'Python'];

//Deklarasi function
function fx(array, inifunction){
    const newArray = [];
    
    for(let i = 0; i < array.length; i++){
        newArray.push(inifunction(array[i]))
    }

    return newArray
}

//Function digunakan
const untukLength = (item) => {
    return item.length;
}

// console.log(untukLength)
const uFx = fx(strArray, untukLength)
console.log(uFx)