// class namanya Mobil

// static bergerakdidarat = true

// constructor:
// merek:
// warnanya apa:
// manual:(true/false)

class Mobil {
    static begerakdidarat = true;

    constructor(merek, warna, manual){
        this.merek = merek
        this.warna = warna
        this.manual = manual
    }

    getMerek(){
        console.log(this.merek)
        return this.merek
    }

    getWarna(){
        console.log(this.warna)
        return this.warna
    }

    getManual(){
        console.log(this.manual)
        return this.manual
    }

    setMerek(merek){
        this.merek = merek
        console.log(this.merek)
    }

    setWarna(warna){
        this.warna = warna
        console.log(this.warna)
    }
    
    setManual(manual){
        this.manual = manual
        console.log(this.manual)
        return this.manual
    }

    getMobil(){
        let mobil = `Mobil dengan merek ${this.merek} memiliki warna ${this.warna} dan memiliki mekanisme ${this.manual ? 'Manual': 'Otomatis'}`
        console.log(mobil)
        return mobil
    }

    fCustom(inifungsi){
        return inifungsi(this)
    }
}

let mobilOtomatis = new Mobil('Honda', 'Merah', false)
mobilOtomatis.getMobil()

console.log('Ambil Warnanya aja: ', mobilOtomatis.getWarna())

let fungsi = (obj) => {
    obj.getMobil()
    console.log(`Saya punya mobil warna ${obj.warna}`)
};

mobilOtomatis.fCustom(fungsi)

console.log('<----------------------')

mobilManual = new Mobil('Toyota', 'Hitam', true)
mobilManual.getMobil()

console.log('Ambil Mereknya aja: ', mobilOtomatis.merek)

