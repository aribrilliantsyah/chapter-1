class Human {

    constructor(nama){
        this.nama = nama;
    }

    static tanyaKabar(nama){
        console.log(`Hallo ${nama}, bagaimana kabar mu?`)
    }   

    ucapkanTerimakasih(){
        console.log(`Terima kasih ${this.nama} _/\_`)
    }
    
}

let ujang = new Human('Mimin')
ujang.ucapkanTerimakasih()

Human.tanyaKabar('Amir')