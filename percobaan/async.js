let persediaanKopi = 3;

const pesanKopi = (nama) => new Promise((resolve, reject) => {
    if(persediaanKopi > 0){
        persediaanKopi--;
        resolve(`${nama} berhasil memesan kopi.`);
    }else{
        reject(`${nama} kehabisan kopi, jadi gagal memesan kopi.`)
    }
})

let ariPesanKopi = pesanKopi('Ari').then((res) => {
    console.log(res)
}).catch((err) => {
    console.log(err)
})

let ujangPesanKopi = pesanKopi('Ujang').then((res) => {
    console.log(res)
}).catch((err) => {
    console.log(err)
})

let asepPesanKopi = pesanKopi('Asep').then((res) => {
    console.log(res)
}).catch((err) => {
    console.log(err)
})

let ratnaPesanKopi = pesanKopi('Ratna').then((res) => {
    console.log(res)
}).catch((err) => {
    console.log(err)
})
