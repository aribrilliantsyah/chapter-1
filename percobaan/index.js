const fs = require('fs')
const option = { encoding: "utf-8" }

const callback = (err, data) => {
    console.log("Aku muncul kedua")
    if (err) return console.error("Error: ", err.message)
    console.log("Is File:", data)
}

const fileJanji = (file) => new Promise((resolve, reject) => {
    fs.readFile(file, option, (err, data) => {
        if(err) reject(err)
        resolve(data)
    })
})

// fs.readFile("contoh.txt", option, callback)

// console.log("Aku muncul pertama")

// let test = async() => {
//     let data = await fileJanji('contoh.txt')
//     console.log('File lewat async-await', data)
// }

// test()

fileJanji('novel.txt').then((data) => {
    console.log('File lewat janji\n')
    console.log(data)
}).catch((err) => {
    console.error('Error lewat janji', err)
})


