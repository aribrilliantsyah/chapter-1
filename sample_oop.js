
class Excel {
    static A_RIGHT = 'text-align: right';
    static A_LEFT = 'text-align: left';
    static A_CENTER = 'text-align: center';
    static A_JUSTIFY = 'text-align: justify';

    setAlign(align){
        this.align = align
    }

    printData(){
        if(this.align == this.A_RIGHT ||
            this.align == this.A_LEFT ||
            this.align == this.A_CENTER ||
            this.align == this.A_JUSTIFY){
            console.log("Invalid Property");return;
        }
        console.log(`Data memiliki property align: ${this.align}`)
    }
}

let newData = new Excel()
newData.setAlign(Excel.A_CENTER)
newData.printData()